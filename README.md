# My project's README

**Dependencies**

* jsoup
* json-simple
* junit

**How to run the APP**

* Clone the project
* From the main folder -> *mvn compile* and *mvn exec:java*

**How to run the TEST**

* mvn compile and mvn test