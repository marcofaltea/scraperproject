package com.scraper;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * Test for simple App.
 */
public class AppTest 
{
	    Scraper scraper;
	    private static final Logger LOG = Logger.getLogger(AppTest.class.getName());

	    public AppTest() {
	    }

	    @Before
	    public void setUp() {
	        URL url;
	        try {
	            url = new URL("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html");
	            scraper = new Scraper();
	            scraper.setUrl(url);
	        } catch (MalformedURLException ex) {
	        	LOG.log(Level.SEVERE, "Problem during Scraper test");
	        }
	    }

	    @After
	    public void cancellScraper() {
	        scraper = null;
	    }

	    @Test
	    public void testScrapeNotEmpty() throws IOException {
	        String json = scraper.scrape();
	        assertTrue(!json.isEmpty());
	    }
	    @Test
	    public void testGetProductDataNull() {
	        String url = "http://www.google.it/";
	        ProductData product = scraper.getProduct(url);
	        assertNull(product);
	    }
	    @Test
	    public void testGetProductData() {
	        String adr = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-avocado-xl-pinkerton-loose-300g.html";
	        ProductData product = scraper.getProduct(adr);
	        assertNotNull(product);
	        assertNotNull(product.getTitle());
	    }

	    @Test
	    public void testScraper() throws IOException {
	        String json = scraper.scrape();
	        assertTrue(json.contains("Avocado"));
	    }


}
