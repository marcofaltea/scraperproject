package com.scraper;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class Scraper {
	
	  private static final String UL_PRODUCT_LISTER = "ul.productLister";
	  private static final String DIV_PRODUCT_INFO = "div.productInfo";
	  private static final String DIV_PRODUCT_TEXT = "div.productText";
	  private static final String P_PRICE_PER_UNIT = "p.pricePerUnit";
	  private static final String DIV_PRODUCT_TITLE_DESCRIPTION_CONTAINER = "div.productTitleDescriptionContainer";
	  
	  private URL url;
	  private Connection connection;
	  private static final Logger LOG = Logger.getLogger(Scraper.class.getName());
	  
	  /**
	   * Start scraping process
	   * 
	   * @return String with JSON content
	   * 
	   */
		public String scrape() throws IOException {
	    	JSONObject json = new JSONObject();
	        JSONArray results = new JSONArray();
	        json.put("results", results);
	        float totalUnitPrice = 0.0f; 
	        
	       Connection con = Jsoup.connect(url.toString());
	        if (con == null) {
				LOG.log(Level.SEVERE, "Connection problem, please check your interet connection and try again");
				 return "{}";
	        }

            Element ulElement = con.get().select(UL_PRODUCT_LISTER).first();
            if (ulElement == null) {
            	LOG.log(Level.SEVERE, "No list of products, please change Url");
            	return "{}";
            }
	        totalUnitPrice = populateJson(results, totalUnitPrice, con, ulElement);
	        json.put("totalUnitPrice", totalUnitPrice);
	        
	        return json.toJSONString();
	    }

		/**
		 * Populate Json with product info and return a total price
		 *  
		 * @param results
		 * @param totalUnitPrice
		 * @param con
		 * @param ulElement
		 * @return
		 */
		@SuppressWarnings("unchecked")
		private float populateJson(JSONArray results, float totalUnitPrice, Connection con, Element ulElement) {
			Elements els = ulElement.getElementsByTag("li");
			for (Element element: els) {
			    Element divProductInfo = element.select(DIV_PRODUCT_INFO).first();
			    Element linkElement = divProductInfo.getElementsByTag("a").first();
			    
			    String infoUrl = linkElement.attr("href");
			    ProductData productInfo = getProduct(infoUrl);
			    results.add(productInfo.toJSON());
			    totalUnitPrice += productInfo.getUnitPrice();
			}
			return totalUnitPrice;
		} 
		/**
		 * Create connection, populate and return ProductData
		 * @param infoUrl
		 * @return ProductData
		 */
	    public ProductData getProduct(String infoUrl) {
	        String title = "";
	        double size = 0.0;
	        double unitPrice = 0.0;
	        String description = "";
	        
	        try {
	            Document doc = Jsoup.connect(infoUrl).get();
	            Element titleElement = doc.select(DIV_PRODUCT_TITLE_DESCRIPTION_CONTAINER).first();
	            if (titleElement == null) {
	                return null;
	            } else {
		            // Page Size
		            size = doc.toString().length() / 1024;
		            // Product Title
		            title = titleElement.getElementsByTag("h1").first().text();
		            // Price
	            }
		        Element priceElement = doc.select(P_PRICE_PER_UNIT).first();
		            if (priceElement == null) {
		                return null;
		            } else {
		            	// Unit Price
		                unitPrice = Double.valueOf(priceElement.text().replace("/unit", "").replace("£", ""));
		            }
		        Element descriptionElement = doc.select(DIV_PRODUCT_TEXT).first();
		            if (descriptionElement == null) {
		                return null;
		            } else {
		            	// Description
		                description = descriptionElement.text();
		            }
	        } catch (IOException ex) {
	        	LOG.log(Level.SEVERE, "Problem during parsing page");
	        }
	        
	        return new ProductData(title, size, unitPrice, description);
	    }
	    
	public URL getUrl() {
		return url;
	}
	public void setUrl(URL url) {
		this.url = url;
	}
	public Connection getConnection() {
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
}