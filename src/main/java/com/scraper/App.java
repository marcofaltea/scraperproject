package com.scraper;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class that scrap webpage
 * 
 * @author marcoaltea 
 *
 */
public class App 
{
    private static final String DEFAULT_URL = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html";

    private static final Logger LOG = Logger.getLogger(App.class.getName());
    
    /**
     * Main method that scrape webpage find from DEFAULT_URL
     * @param args
     * @throws MalformedURLException
     */
	public static void main( String[] args ) throws MalformedURLException
    {
    
    	URL url = new URL(DEFAULT_URL);
    	Scraper scraper = new Scraper();
    	scraper.setUrl(url);
    	try {
			System.out.println(scraper.scrape());
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Problem during the scraper of the page", e);
		}
    
    }
    
}