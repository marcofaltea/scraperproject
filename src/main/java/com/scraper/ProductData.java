package com.scraper;

import org.json.simple.JSONObject;

/**
 * Data Product class where to store data to scrap
 * 
 * @author marcoaltea
 *
 */
public class ProductData {
		private String title;
	    private double size;
	    private double unitPrice;
	    private String description;

	    public ProductData(String title, double size, double unitPrice, String description) {
	        this.title = title;
	        this.size = size;
	        this.unitPrice = unitPrice;
	        this.description = description;
	    }
	    
	    @SuppressWarnings("unchecked")
		public JSONObject toJSON() {
	        JSONObject json = new JSONObject();
	        json.put("title", title);
	        json.put("description", description);
	        json.put("size", size);
	        json.put("price", unitPrice);
	        return json;
	    }

	    public String getTitle() {
	        return title;
	    }

	    public void setTitle(String title) {
	        this.title = title;
	    }

	    public double getSize() {
	        return size;
	    }

	    public void setSize(float size) {
	        this.size = size;
	    }

	    public double getUnitPrice() {
	        return unitPrice;
	    }

	    public void setUnitPrice(float unitPrice) {
	        this.unitPrice = unitPrice;
	    }

	    public String getDescription() {
	        return description;
	    }

	    public void setDescription(String description) {
	        this.description = description;
	    }

	    @Override
	    public String toString() {
	        return "Product{" + "title=" + title + ", size=" + size + ", unitPrice=" + unitPrice + ", description=" + description + '}';
	    }
	}
